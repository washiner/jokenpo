import 'dart:math';

import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  var _imagemApp = AssetImage("lib/assets/padrao.png");
  var _messagem = "Escolha uma opção abaixo";

  void _opcaoSelecionada(String escolhaUsuario){

    var opcoes = ["pedra", "papel", "tesoura"];
    var numero = Random().nextInt(3);
    var escolhaApp = opcoes[numero];

    switch(escolhaApp){
      case "pedra":
        setState(() {
          this._imagemApp = AssetImage("lib/assets/pedra.png");
        });
        break;
      case "papel":
        setState(() {
          this._imagemApp = AssetImage("lib/assets/papel.png");
        });
        break;
      case "tesoura":
        setState(() {
          this._imagemApp = AssetImage("lib/assets/tesoura.png");
        });
        break;
    }
    //validacao do ganhador
    // usuario ganhador
    if(
    (escolhaUsuario == "pedra" && escolhaApp == "tesoura") ||
    (escolhaUsuario == "tesoura" && escolhaApp == "papel") ||
    (escolhaUsuario == "papel" && escolhaApp == "pedra")
    ){

      setState(() {
        _messagem = "Parabéns você ganhou !!!!!";
      });
      //app ganhador
    }else if(
    (escolhaApp == "pedra" && escolhaUsuario == "tesoura") ||
    (escolhaApp == "tesoura" && escolhaUsuario == "papel") ||
    (escolhaApp == "papel" && escolhaUsuario == "pedra")
    ){
      setState(() {
        _messagem = "Você Perdeu... sorry";
      });
    }else{
      setState(() {
        _messagem = " Empatamos ...";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      title: Text("Jaquem Pô"),
    ),
      body: Column(
        children: [
          //text
          Padding(
            padding: const EdgeInsets.only(top: 32, bottom: 18),
            child: Text(
              "Escolha do APP",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
          ),
          //imagem
          Image(image: _imagemApp),
          //text resultado
          Padding(
            padding: const EdgeInsets.only(top: 32, bottom: 18),
            child: Text(
              _messagem,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
          ),
          // linha 3 imagem
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                onTap: () => _opcaoSelecionada("pedra"),
                child: Image.asset("lib/assets/pedra.png", height: 120,),
              ),
              GestureDetector(
                onTap: () => _opcaoSelecionada("papel"),
                child: Image.asset("lib/assets/papel.png", height: 120,),
              ),
              GestureDetector(
                onTap: () => _opcaoSelecionada("tesoura"),
                child: Image.asset("lib/assets/tesoura.png", height: 120,),
              ),
          ],)
        ],
      ),
    );
  }
}
